from mezzanine.conf import register_setting

register_setting(
    name="MAILCHIMP_LIST_ID",
    description="List Id de Mailchimp.",
    editable=True,
    default='',
)

register_setting(
    name="MAILCHIMP_API_KEY",
    description="Api Key de Mailchimp.",
    editable=True,
    default='',
)

register_setting(
    name="MAILCHIMP_FORM_SIGNAL",
    description="Connect Mailchimp subscribe action to mezzanine valid form signal.",
    editable=True,
    default=True,
)